

See http://www.campaignmonitor.com/guides/coding/guidelines/ for complete guide.

But Basically:

- Use inline styles
- Always set width, height, and alt attributes on images
- All images should be hosted on the same server, not in a folder
- Apply cellspacing="0", cellpadding="0", style="border-collapse: collapse;" on all table elements
- Apply widths on td's not table, or any other element for that matter
- padding and margins do not work in outlook, use spacer gifs instead
- Some clients strip html, head, and body tags - so dont put anything in the head